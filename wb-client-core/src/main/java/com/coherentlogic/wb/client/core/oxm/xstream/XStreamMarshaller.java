package com.coherentlogic.wb.client.core.oxm.xstream;

import com.coherentlogic.wb.client.core.domain.AdminRegion;
import com.coherentlogic.wb.client.core.domain.CatalogSource;
import com.coherentlogic.wb.client.core.domain.CatalogSources;
import com.coherentlogic.wb.client.core.domain.Countries;
import com.coherentlogic.wb.client.core.domain.Country;
import com.coherentlogic.wb.client.core.domain.DataPoint;
import com.coherentlogic.wb.client.core.domain.DataPointCountry;
import com.coherentlogic.wb.client.core.domain.DataPointIndicator;
import com.coherentlogic.wb.client.core.domain.DataPoints;
import com.coherentlogic.wb.client.core.domain.ErrorMessage;
import com.coherentlogic.wb.client.core.domain.IncomeLevel;
import com.coherentlogic.wb.client.core.domain.IncomeLevelCodes;
import com.coherentlogic.wb.client.core.domain.IncomeLevels;
import com.coherentlogic.wb.client.core.domain.Indicator;
import com.coherentlogic.wb.client.core.domain.IndicatorSource;
import com.coherentlogic.wb.client.core.domain.IndicatorTopic;
import com.coherentlogic.wb.client.core.domain.IndicatorTopics;
import com.coherentlogic.wb.client.core.domain.Indicators;
import com.coherentlogic.wb.client.core.domain.LendingType;
import com.coherentlogic.wb.client.core.domain.LendingTypeCodes;
import com.coherentlogic.wb.client.core.domain.LendingTypes;
import com.coherentlogic.wb.client.core.domain.Message;
import com.coherentlogic.wb.client.core.domain.Region;
import com.coherentlogic.wb.client.core.domain.RegionCodes;
import com.coherentlogic.wb.client.core.domain.Source;
import com.coherentlogic.wb.client.core.domain.Topic;
import com.coherentlogic.wb.client.core.domain.Topics;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.security.NoTypePermission;

/**
 * 
 */
public class XStreamMarshaller extends org.springframework.oxm.xstream.XStreamMarshaller {

    /**
     * @see https://groups.google.com/forum/#!topic/xstream-user/wiKfdJPL8aY
     * 
     * @throws com.thoughtworks.xstream.security.ForbiddenClassException
     */
    @Override
    protected void customizeXStream(XStream xstream) {

        super.customizeXStream(xstream);

        xstream.addPermission(NoTypePermission.NONE);

        xstream.allowTypes(
            new Class[] {
                AdminRegion.class,
                CatalogSource.class,
                CatalogSources.class,
                Countries.class,
                Country.class,
                DataPoint.class,
                DataPointCountry.class,
                DataPointIndicator.class,
                DataPoints.class,
                ErrorMessage.class,
                IncomeLevel.class,
                IncomeLevelCodes.class,
                IncomeLevels.class,
                Indicator.class,
                Indicators.class,
                IndicatorSource.class,
                IndicatorTopic.class,
                IndicatorTopics.class,
                LendingType.class,
                LendingTypes.class,
                LendingTypeCodes.class,
                Message.class,
                Region.class,
                RegionCodes.class,
                Source.class,
                Topic.class,
                Topics.class
            }
        );
    }
}
