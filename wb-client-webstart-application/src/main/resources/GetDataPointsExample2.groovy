/* This example, which is written in Groovy script, sends a request to the
 * following URI:
 *
 * http://api.worldbank.org/countries/all/indicators/SP.POP.TOTL?date=2000:2002
 *
 * @see http://databank.worldbank.org/data/home.aspx
 * @see http://datacatalog.worldbank.org/
 * @see http://data.worldbank.org/developers
 */

import joinery.DataFrame
import joinery.DataFrame.PlotType

def results = queryBuilder
    .countries("all")
    .indicators("SP.POP.TOTL")
    .setDate("1980:2015")
    .setPage(2)
    .doGetAsDataPoints ()

def dataFrame = new DataFrame<Object> ()

def dates = [] as List
def values = [] as List<BigDecimal>

def sorted = results.dataPointList.sort { it.date }

sorted.forEach {
  // B8 = Central Europe and the Baltics
  if (it.date != null && it.value != null  && it.country.id == "B8") {
    dates << new java.text.SimpleDateFormat ("yyyy-MM-dd").parse(it.date + "-01-01")
    values << new BigDecimal (it.value)
  }
}

dataFrame.add((Object) "Date", (List<Object>) dates)
dataFrame.add((Object) "Values", (List<Object>) values)

log.info ("dates: $dates")
log.info ("values: $values")

dataFrame.plot(PlotType.LINE_AND_POINTS)
dataFrame.show()

return results